DROP TABLE participe;
DROP TABLE import;
DROP TABLE Athlete;
DROP TABLE Sport;
DROP TABLE Region;
DROP TABLE evenement;

CREATE temp TABLE import(
    id INT,
    name TEXT,
    sex CHAR(1),
    age INT,
    height FLOAT,
    weight floaT,
    team text,
    noc CHAR(3),
    games text,
    year INT,
    season text,
    city text,
    sport text,
    event TEXT,
    medal text);

\copy import FROM athlete_events.csv delimiter ',' CSV HEADER NULL 'NA'

DELETE FROM import WHERE year<1920 OR sport='Art Competitions';


DROP TABLE regions;
CREATE TABLE regions(
    noc CHAR(3),
    region TEXT,
    notes TEXT
);

\copy regions FROM noc_regions.csv delimiter ',' CSV HEADER NULL 'NA'

UPDATE regions SET noc = 'SGP' WHERE noc = 'SIN';


--Exercice 4

\echo Création de la table Athlete

CREATE TABLE Athlete(
    id_athlete SERIAL,
    name TEXT,
    sex CHAR(1),
    CONSTRAINT pk_athlete PRIMARY KEY(id_athlete)
);

\echo Création de la table Region

CREATE TABLE Region(
    team TEXT,
    noc CHAR(3),
    CONSTRAINT pk_region PRIMARY KEY(noc)
);

\echo Création de la table Sport

CREATE TABLE Sport(
    sport TEXT,
    event TEXT,
    CONSTRAINT pk_sport PRIMARY KEY(sport, event)
);

\echo Création de la table evenement
CREATE TABLE evenement(
    year INTEGER,
    season CHAR(6),
    city TEXT, 
    CONSTRAINT pk_evenement PRIMARY KEY(year, season, city)
);

\echo Création de la table participe

CREATE TABLE participe(
    id_athlete INTEGER,
    year INTEGER,
    season CHAR(6),
    city TEXT,
    sport TEXT,
    event TEXT,
    noc CHAR(3),
    medal CHAR(6),
    age INTEGER,
    weight FLOAT,
    height INT,

    CONSTRAINT pk_participe PRIMARY KEY(id_athlete, year, season, city, sport, event, noc),
    
    CONSTRAINT fk_athlete FOREIGN KEY(id_athlete)
    REFERENCES Athlete(id_athlete)
    ON UPDATE CASCADE,
    
    CONSTRAINT fk_evenement FOREIGN KEY(year, season, city)
    REFERENCES evenement(year, season, city)
    ON UPDATE CASCADE,

    CONSTRAINT fk_sport FOREIGN KEY(sport, event)
    REFERENCES Sport(sport, event)
    ON UPDATE CASCADE,

    CONSTRAINT fk_region FOREIGN KEY(noc)
    REFERENCES Region(noc)
    ON UPDATE CASCADE
);

\echo Insertion des données

INSERT INTO Athlete(id_athlete, name, sex) (SELECT DISTINCT id,name,sex FROM import);
INSERT INTO Region(team, noc) (SELECT DISTINCT regions.region, import.noc FROM regions INNER JOIN import USING(noc));
INSERT INTO Sport(sport, event) (SELECT DISTINCT sport, event FROM import);
INSERT INTO evenement(year, season, city) (SELECT DISTINCT year, season, city FROM import);
INSERT INTO participe(id_athlete, year, season, city, sport, event, noc, medal, age, weight, height) (SELECT id, year, season, city, sport, event, noc, medal, age, weight, height FROM import);
 