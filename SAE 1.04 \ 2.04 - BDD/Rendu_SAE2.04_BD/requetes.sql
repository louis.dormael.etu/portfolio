-- EXERCICE 3 

-- Question 1
SELECT COUNT(*) FROM INFORMATION _SCHEMA.COLUMNS WHERE TABLE_NAME = 'import';

-- Question 2 
SELECT COUNT(*) FROM import;

-- Question 3 
SELECT COUNT(*) FROM regions;

-- Question 4 
SELECT COUNT(DISTINCT id) FROM import;

--Questions 5
SELECT COUNT(*) FROM import WHERE medal='Gold';

--Question 6
SELECT COUNT(*) FROM import WHERE name LIKE '%Carl Lewis%';




--Exercice 5

--Question 1 
SELECT Region.team, COUNT(participe.noc) AS nombre_participations 
FROM participe INNER JOIN Region USING(noc) 
GROUP BY Region.team 
ORDER BY nombre_participations DESC;

--Question 2
SELECT Region.team, COUNT(participe.medal) AS nombre_MedaillesOr
FROM participe INNER JOIN Region USING(noc) 
WHERE medal='Gold' 
GROUP BY Region.team 
ORDER BY nombre_MedaillesOr DESC;

--Question 3
SELECT Region.team, COUNT(participe.medal) AS nombre_Medailles 
FROM participe INNER JOIN Region USING(noc) 
WHERE medal!='' 
GROUP BY Region.team 
ORDER BY nombre_Medailles DESC;

--Question 4
SELECT participe.id_athlete, Athlete.name, COUNT(participe.medal) AS nombre_MedaillesOr 
FROM participe INNER JOIN Athlete USING(id_athlete) 
WHERE medal='Gold' 
GROUP BY participe.id_athlete, Athlete.name 
ORDER BY nombre_MedaillesOr DESC;

--Question 5
SELECT noc, COUNT(*) AS nombre_MédaillesOr 
FROM participe 
WHERE medal !='' AND city='Albertville' 
GROUP BY noc 
ORDER BY nombre_MédaillesOr DESC;

--Question 6
SELECT COUNT(DISTINCT p1.id_athlete) 
FROM participe AS p1 INNER JOIN participe AS p2 USING(id_athlete) 
WHERE p1.year < p2.year AND p1.noc != 'FRA' AND p2.noc = 'FRA';

--Question 7
SELECT COUNT(DISTINCT p1.id_athlete) 
FROM participe AS p1 INNER JOIN participe AS p2 USING(id_athlete) 
WHERE p1.year < p2.year AND p1.noc = 'FRA' AND p2.noc != 'FRA';


--Question 8
SELECT age, COUNT(medal) AS MédaillesOr 
FROM participe 
WHERE medal='Gold' 
GROUP BY age;

--Question 9
SELECT event, COUNT(medal) AS Médailles 
FROM participe 
WHERE medal!='' AND age>50 
GROUP BY event 
ORDER BY Médailles DESC;

--Question 10
SELECT evenement.year, evenement.season, COUNT(participe.event) AS Nombre_discipline 
FROM evenement INNER JOIN participe USING(year,season,city) 
GROUP BY evenement.year, evenement.season 
ORDER BY evenement.year DESC;

--Question 11
SELECT year, COUNT(medal) 
FROM participe 
WHERE medal !='' AND event LIKE '%Women%' AND season ='Summer' 
GROUP BY evenement.year;



--Exercice 6

-- PAYS : FRANCE
-- SPORT : BOBSLEIGH

--Les différentes types d'épreuves de Bobsleigh auquel la France a participé
SELECT DISTINCT(event) FROM participe
WHERE event LIKE '%Bobsleigh%' AND noc = 'FRA';

--Le nombre de participation aux épreuves de Bobsleigh de la France. 
SELECT COUNT(*) AS Nombre_participation FROM participe 
WHERE event LIKE '%Bobsleigh%' AND noc = 'FRA';

--Les informations concernant la première participation de la France à une épreuve de Bobsleigh
SELECT * FROM participe 
WHERE event LIKE '%Bobsleigh%' AND noc = 'FRA' 
AND year <= ALL(SELECT year FROM participe 
                WHERE event LIKE '%Bobsleigh%' AND noc = 'FRA');

--Les médaillés français aux épreuves ainsi que leur nombre de médailles par ordre décroissant.
SELECT Athlete.name, COUNT(participe.medal) AS nombre_médailles 
FROM participe INNER JOIN Athlete USING(id_athlete) 
WHERE participe.noc = 'FRA' AND participe.event LIKE '%Bobsleigh%' 
AND participe.medal != '' GROUP BY Athlete.name ORDER BY nombre_médailles DESC;


