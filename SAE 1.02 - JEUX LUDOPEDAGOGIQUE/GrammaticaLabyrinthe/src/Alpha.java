import extensions.File;
import extensions.CSVFile;


class Grammaticalabyrinth extends Program{


    /////////////////////////////////////////// -Variables Globales- ////////////////////////////////////////////////////////////////

    final int[] POSITION_INITIAL_JOUEUR = new int[]{1,1};
    final int NOMBRES_VIES_NORMAL_DIFFICILE = 3;
    final int NOMBRES_VIES_ULTIME = 1;
    final char SYMBOLE_JOUEUR = '*';
    final double PROBA_MONSTRE_NORMALE = 0.08;
    final double PROBA_MONSTRE_DIFFICILE = 0.1;
    final double PROBA_MONSTRE_ULTIME = 0.14;
    final double PROBA_COFFRE_NORMALE = 0.05;
    final double PROBA_COFFRE_DIFFICILE = 0.03;
    final double PROBA_COFFRE_ULTIME = 0.0;
    final char SYMBOLE_MONSTRE = '!';
    final char SYMBOLE_COFFRE = 'C';
    final String COFFRE = "../Ressources/Coffre/coffre.txt";
    final String monstre1 = "../Ressources/Monstres/monstre1.txt";
    final String monstre2 = "../Ressources/Monstres/monstre2.txt";
    final String monstre3 = "../Ressources/Monstres/monstre3.txt";
    final CSVFile CSV_MONSTRE = loadCSV("../Ressources/csv/phrase.csv");
    final CSVFile CSV_COFFRE = loadCSV("../Ressources/csv/homonymes.csv");
    final String RESET = "\033[0m";      // Text Reset
    final String BLACK = "\033[0;30m";   // BLACK
    final String RED = "\033[0;31m";     // RED
    final String GREEN = "\033[0;32m";   // GREEN
    final String YELLOW = "\033[0;33m";  // YELLOW
    final String BLUE = "\033[0;34m";    // BLUE
    final String PURPLE = "\033[0;35m";  // PURPLE
    final String CYAN = "\033[0;36m";    // CYAN
    final String WHITE = "\033[0;37m";   // WHITE




    //////////////////////////////////////////// -void algorithm- /////////////////////////////////////////////////////////////////

    void algorithm(){
    
        String mode = menuPrincipal();
        if(equals(mode, "jouer")){
            String difficulte = jouer();
            int vie = 0;
            double spawnMonstre = 0.0;
            double spawnCoffre = 0.0;
            String filename = "";

            if(equals(difficulte, "normal")){
                vie = NOMBRES_VIES_NORMAL_DIFFICILE;
                spawnMonstre = PROBA_MONSTRE_NORMALE;
                spawnCoffre = PROBA_COFFRE_NORMALE;
                filename = labyrintheNormal();
            }
            
            if(equals(difficulte, "difficile")){
                vie = NOMBRES_VIES_NORMAL_DIFFICILE;
                spawnMonstre = PROBA_MONSTRE_DIFFICILE;
                spawnCoffre = PROBA_COFFRE_DIFFICILE;
                filename = labyrintheDifficile();
            }

            if(equals(difficulte, "ultime")){
                vie = NOMBRES_VIES_ULTIME;
                spawnMonstre = PROBA_MONSTRE_ULTIME;
                spawnCoffre = PROBA_COFFRE_ULTIME;
                filename = labyrintheUltime();
            }

            Joueur j = creerJoueur(vie);
            char[][] labyrinthe = creationLabyrinthe(filename, j);
            ajouterMonstre(labyrinthe,spawnMonstre);
            ajouterCoffre(labyrinthe,spawnCoffre);
            afficherLabyrinth(labyrinthe);
            afficherVie(j.nbVies);
            int[] victoire = positionVictoire(labyrinthe);
            
            while(j.nbVies!=0 && !estPositionVictoire(j.position, victoire)){
                String deplacement = obtenirDeplacement(labyrinthe, j);
                
                if(estUnEnnemi(labyrinthe, j, deplacement) == true){
                    int n = 0;
                    n = obtenirLigneAleatoireMonstre();
                    String type = obtenirTypePhraseMonstre(n, CSV_MONSTRE);
                    String phrase = obtenirPhraseMonstre(n, CSV_MONSTRE);
                    String[] tabReponse = obtenirTabReponse(type);
                    rencontreMonstre(type, phrase, j);
                }
                
                if(estUnCoffre(labyrinthe, j, deplacement) == true){
                    int n = 0;
                    n = obtenirLigneAleatoireCoffre();
                    String homonymes = obtenirHomonymeCoffre(n, CSV_COFFRE);
                    String motManquant = obtenirMotManquantCoffre(n, CSV_COFFRE);
                    String phrase = obtenirPhraseCoffre(n, CSV_COFFRE);
                    rencontreCoffre(homonymes, motManquant, phrase, j);
                }
                
                deplacementJoueur(labyrinthe, j, deplacement);
                afficherLabyrinth(labyrinthe);
                afficherVie(j.nbVies);
                println();
            }
            if(estPositionVictoire(j.position, victoire)){
                println();
                println(GREEN+"Bravo ! vous avez réussi à sortir du labyrinthe"+RESET);
                println();
            }
            else{
                println();
                println(RED+"Vous avez perdu toutes vos vie ! GAME OVER."+RESET);
                println();
            }
        }

        if(equals(mode, "options")){
            options();
        }

        if(equals(mode, "quitter")){
        }

    }
    /////////////////////////////// -Menus Principaux (à compléter)- /////////////////////////////////////////
    
    String menuPrincipal(){
        String mode="";
        println(RED+"======================================");
        println("Bienvenue sur Grammatical Labyrinth !");
        println("======================================"+RESET);
        println();
        println("Que voulez-vous faire ?");
        println();
        println("1. JOUER");
        println("2. OPTIONS");
        println("3. QUITTER");
        println();
        int choixJoueur=-1;
        while(choixJoueur != 1 && choixJoueur != 2 && choixJoueur != 3){
            println();
            println("Chossissez entre 1 et 3 : ");
            choixJoueur = readInt();
        }
        if(choixJoueur == 1){
            mode = "jouer";
        }
        if(choixJoueur == 2){
            mode = "options";
        }
        if(choixJoueur == 3){
            mode = "quitter";
        }
        return mode;
        
    }
 
    String jouer(){
        String mode="";
        println();
        println(RED+"Choississez le niveau de difficulté : "+RESET);
        println();
        println("1. Entrainement Basique");
        println("2. Entrainement amélioré");
        println("3. Entrainement ULTIME");
        println();
        int choixJoueur=0;
        while(choixJoueur != 1 && choixJoueur != 2 && choixJoueur != 3){
            println("Choississez entre 1 et 3 : ");
            choixJoueur = readInt();
        }
        if(choixJoueur==1){
            mode = "normal";
        }
        if(choixJoueur==2){
            mode = "difficile";
        }
        if(choixJoueur==3){
            mode = "ultime";
        }
        return mode;
    }
 
    void options(){
        String mode=""; 
        println("1. Règles du jeu");
        println("2. Commandes");
        println("3. Retour au Menu Principal");
        println();
        int choixJoueur=0;
        while(choixJoueur != 1 && choixJoueur != 2 && choixJoueur != 3){
            choixJoueur = readInt();
        }
        if(choixJoueur == 1){
            int choix;
            println(RED+"Règles du jeu"+RESET);
            println("Ce jeu présente une série de labyrinthes en choisissant parmis plusieurs difficultés dont le but est de s'échapper. ");
            println("ils seront remplis de monstres symblisés par un 'Ⰻ' qu'il faut battre en répondant à des règles de grammaire.");
            println("Votre personnage et sa position dans le labyrinthe sont symbolisés par '*'");
            println("Vous avez trois vies et chaques fautes effectuées lors d'une tentative de réponse sera traduite par la perte d'une vie.");
            println();
            println("Il est possible de récupérer une vie en répondant correctement à une question posée par un coffre, lui-même symbolisé par un '❤'.");
            println("Si la réponse est fausse, le joueur perd la possibilité de récupérer une vie, jusqu'à l'apparition d'un nouveau coffre.");
            println();
            println("Pour retourner au menu principal, entrez 1.");
            choix = readInt();
            while(choix !=1){
                println();
                println("Pour retourner au menu principal, entrez 1.");
                choix = readInt();
            }
            menuPrincipal();
        }
        if(choixJoueur == 2){
            int choix;
            println("Commandes");
            println("La touche ou la lettre 'z' permet de se déplacer vers le haut.");
            println("La touche ou la lettre 'q' permet de se dépalcer vers la gauche.");
            println("La touche ou la lettre 's' permet de se déplacer vers la bas.");
            println("La touche ou la lettre 'd' permet de se déplacer vers la droite.");
            println();
            println("Pour retourner au menu principal, entrez 1.");
            choix = readInt();
            while(choix !=1){
                println();
                println("Pour retourner au menu principal, entrez 1.");
                choix = readInt();
            }
            menuPrincipal();
            
        }
        if (choixJoueur == 3){
            println("Retour au Menu Principal");
            menuPrincipal();
        }

    }

    /////////////////////// -Création de classe- //////////////////////

    Joueur creerJoueur(int vie){
        Joueur player = new Joueur();
        player.nbVies = vie;
        player.position = POSITION_INITIAL_JOUEUR;
        return player;
    }

    Monstre creerMonstre(String type, String phrase){
        Monstre m = new Monstre();
        m.phrase = phrase ;
        m.type = type;
        return m;
    }

    Coffre creerCoffre(String homonymes, String motManquant, String phrase){
        Coffre c = new Coffre();
        c.homonymes = homonymes;
        c.motManquant = motManquant;
        c.phrase = phrase;
        return c;
    }


    ///////////////////////////// -Fichiers CSV- /////////////////////////////

    int obtenirLigneAleatoireMonstre(){
        int n = (int)(random()*41);
        if(n==0){
            n=2;
        }
        else if(n==1){
            n=3;
        }
        return n;
    }

    String obtenirTypePhraseMonstre(int n, CSVFile table){
        String type = getCell(table,n,0);
        return type;
    }

    String obtenirPhraseMonstre(int n, CSVFile table){
        String phrase = getCell(table,n,1);
        return phrase;
    }

    int obtenirLigneAleatoireCoffre(){
        int n = (int)(random()*30);
        return n;
    }
    
    String obtenirHomonymeCoffre(int n, CSVFile table){
        String motManquant = getCell(table,n,0);
        return motManquant;
    }

    String obtenirMotManquantCoffre(int n, CSVFile table){
        String motManquant = getCell(table,n,1);
        return motManquant;
    }

    String obtenirPhraseCoffre(int n, CSVFile table){
        String phrase = getCell(table,n,2);
        return phrase;
    }


    ///////////////////////////////// -Fichiers Texte- //////////////////////////////////

    int largeurFichier(String nomFichier){
        File f = newFile(nomFichier);
        int largeur = 0;
        String currentLine = readLine(f);
        largeur = length(currentLine);
        return largeur;
    }


    int longueurFichier(String nomFichier){
        File f = newFile(nomFichier);
        int longueur = 0;
        while(ready(f)){
            String currentLine = readLine(f);
            longueur++;
        }
        return longueur;
    }

    String labyrintheNormal(){
        int n = (int) (random()*9);
        String labyrinthe = "../Ressources/labyrinthe/NORMAL/labyrinthe"+n+".txt";
        return labyrinthe;
    }

    String labyrintheDifficile(){
        int n = (int) (random()*9);
        String labyrinthe = "../Ressources/labyrinthe/DIFFICILE/labyrinthe"+n+".txt";
        return labyrinthe;
    }

    String labyrintheUltime(){
        int n = (int) (random()*9);
        String labyrinthe = "../Ressources/labyrinthe/ULTIME/labyrinthe"+n+".txt";
        return labyrinthe;
    }


    ///////////////////////  -Création du labyritnhe- //////////////////////////////////////////////////
    
    char[][] creationLabyrinthe(String nomFichier, Joueur j){
        char[][] labyrinthe = new char[longueurFichier(nomFichier)][largeurFichier(nomFichier)];
        File f = newFile(nomFichier);
        int h =-1;
        while(ready(f)){
            h++;
            String currentLine = readLine(f);
            for(int l=0; l < length(labyrinthe, 2); l++){
                labyrinthe[h][l] = charAt(currentLine, l);
            }
        }
        labyrinthe[j.position[0]][j.position[1]] = SYMBOLE_JOUEUR;
        return labyrinthe;  
    }

    char[][] ajouterMonstre(char[][] labyrinthe, double probaMonstre){
        double n = random();
        for(int l=1; l < length(labyrinthe,1)-1; l++){
            for(int c=1; c<length(labyrinthe,2)-1; c++){
                if(labyrinthe[l][c]==' '){
                    if(n<=probaMonstre){
                        labyrinthe[l][c]=SYMBOLE_MONSTRE;
                    }
                }
                n=random();
            }
        }
        return labyrinthe;
    }

    char[][] ajouterCoffre(char[][] labyrinthe, double probaCoffre){
        double n = random();
        for(int l=1; l < length(labyrinthe,1)-1; l++){
            for(int c=1; c<length(labyrinthe,2)-1; c++){
                if(labyrinthe[l][c]==' '){
                    if(n<=probaCoffre){
                        labyrinthe[l][c]=SYMBOLE_COFFRE;
                    }
                }
                n=random();
            }
        }
        return labyrinthe;
    }

    
    void afficherLabyrinth(char[][] labyrinthe){
        println();
        for(int l=0; l<length(labyrinthe, 1); l++){
            for(int c=0; c<length(labyrinthe, 2); c++){
                if(labyrinthe[l][c] == 'O'){
                    print('█');
                }
                else if(labyrinthe[l][c] == '!'){
                    print('Ⰻ');
                }
                else if(labyrinthe[l][c] == 'C'){
                    print('❤');
                }
                else{
                    print(labyrinthe[l][c]);
                }
            }
            println();
        }
        println();
    }

    int[] positionVictoire(char[][] labyrinthe){
        int[] victoire = new int[] {length(labyrinthe, 1) - 2,length(labyrinthe, 2) - 2 };
        return victoire;
    }

    boolean estPositionVictoire(int[] position, int[] victoire){
        if(position[0]==victoire[0] && position[1]==victoire[1]){
            return true;
        }
        return false;
    }


    //////////////////// -Déplacement- //////////////////////////////////////////////



    boolean estDeplacementValide(String deplacement){
        if(!estUnSeulCaractere(deplacement)){
            return false;
        }
        if(charAt(deplacement, 0) == 'z' || charAt(deplacement, 0) == 'q' || charAt(deplacement, 0) == 's' || charAt(deplacement, 0) == 'd'){
            return true;
        }
        return false;
    }


    void testEstDeplacementValide(){
        assertTrue(estDeplacementValide("z"));
        assertTrue(estDeplacementValide("q"));
        assertTrue(estDeplacementValide("s"));
        assertTrue(estDeplacementValide("d"));
        assertFalse(estDeplacementValide("a"));       
    }

    int[] positionFuture(String deplacement, Joueur j){
        int[] positionInitiale = j.position;
        int[] positionFuture = new int[2];
        if(charAt(deplacement,0)=='z'){
            positionFuture[0] = positionInitiale[0]-1;
            positionFuture[1] = positionInitiale[1];
        }
        else if(charAt(deplacement,0)=='s'){
            positionFuture[0] = positionInitiale[0]+1;
            positionFuture[1] = positionInitiale[1];
        }
        else if(charAt(deplacement,0)=='q'){
            positionFuture[0] = positionInitiale[0];
            positionFuture[1] = positionInitiale[1]-1;
        }
        else if(charAt(deplacement,0)=='d'){
            positionFuture[0] = positionInitiale[0];
            positionFuture[1] = positionInitiale[1]+1;
        }
        return positionFuture;
    }

    boolean estDeplacementPossible(char[][] labyrinthe, String deplacement, Joueur j){
        if(!estUnSeulCaractere(deplacement)){
            return false;
        }
        int[] positionFuture = positionFuture(deplacement, j);
        if(labyrinthe[positionFuture[0]][positionFuture[1]] == 'O'){
            return false;
        }
        return true;
    }

    String obtenirDeplacement(char[][] labyrinthe, Joueur j){
        String deplacement = "";
        while((!estDeplacementPossible(labyrinthe, deplacement, j)) && (!estDeplacementValide(deplacement)) && !estUnSeulCaractere(deplacement)){
            println();
            println(RED + "Veuillez entrer un déplacement valide:"+RESET);
            println();
            deplacement = readString();
        }
        return deplacement;
    }

    boolean estUnSeulCaractere(String deplacement){
        boolean bool = false;
        if(length(deplacement) == 1){
            bool = true;
        }
        return bool;
    }

    void deplacementJoueur(char[][] labyrinthe, Joueur j, String deplacement){
        if((estDeplacementPossible(labyrinthe, deplacement, j)) && (estDeplacementValide(deplacement))){
            int[] positionInitiale = j.position;
            int[] positionFuture = positionFuture(deplacement, j);
            labyrinthe[positionFuture[0]][positionFuture[1]] = '*';
            labyrinthe[positionInitiale[0]][positionInitiale[1]] =' ';
            j.position = positionFuture;
        }
    }

    ///////////////////////////////// -Interaction avec les ennemis- ////////////////////////////////////

    boolean estUnEnnemi(char[][] labyrinthe, Joueur j, String deplacement){
        int[] positionFuture = positionFuture(deplacement, j);
        if (labyrinthe[positionFuture[0]][positionFuture[1]] == '!'){
            return true;
        }
        return false;
    }

    boolean estReponseMonstre(String proposition, Joueur j, String[] tabReponses){
        boolean bool = false;
        String reponse = tabReponses[0];
        if(!equals(reponse, proposition)){
            j.nbVies--;
            println();
            println(RED+"Mauvaise Réponse ! Vous perdez un point de Vie. "+RESET);
            println();
        }
        else{
            println();
            println(GREEN+"Bonne Réponse ! Vous avez battu le monstre. "+RESET);
            println();
            bool = true;
        }
        return bool;
    }

    String[] obtenirTabReponse(String type){
        String[] tabReponse = new String[]{type, "affirmative", "négative", "interrogative", "exclamative"};
        return tabReponse;
    }

    void rencontreMonstre(String  type, String phrase, Joueur j){
        Monstre m = creerMonstre(type, phrase);
        String[] tab = new String[]{m.type, "affirmative", "négative", "interrogative", "exclamative"};
        String reponse = tab[0];
        afficherPropositionMonstre(m.phrase);
        println();
        println(RED+"Quelle type de phrase le monstre utilise-t-il ? Répondez en choississant entre 1 et 4."+RESET);
        println();
        int position = readInt();
        while(position!=1 && position!=2 && position!=3 && position!=4){
            println();
            println(RED+"Quelle type de phrase le monstre utilise-t-il ? Répondez en choississant entre 1 et 4."+RESET);
            println();
            position = readInt();
        }
        String proposition = tab[position];
            while(j.nbVies!=0 && !estReponseMonstre(proposition, j, tab)){
                position = 0;
                while(position!=1 && position!=2 && position!=3 && position!=4){
                    println();
                    println(RED+"Quelle type de phrase le monstre utilise-t-il ? Répondez en choississant entre 1 et 4."+RESET);
                    println();
                    position = readInt();
                }
                proposition = tab[position];
            }
        }


    void afficherPropositionMonstre(String phrase){
        int n = (int) (random()*2);
        String monstre = "../Ressources/Monstres/monstre"+n+".txt";
        afficherFichier(monstre);
        println();
        println(BLUE+phrase+RESET);
        println();
        println();
        println("┏ーーーーーーーーーーーーー┓     ┏ーーーーーーーーーーーーー┓    ┏ーーーーーーーーーーーーー┓    ┏ーーーーーーーーーーーーー┓");
        println("|                          |     |                          |    |                          |    |                          |");
        println("|     1. AFFIRMATIVE       |     |       2. NEGATIVE        |    |     3. INTERROGATIVE     |    |     4. EXCLAMATIVE       |");
        println("|                          |     |                          |    |                          |    |                          |");
        println("┗ーーーーーーーーーーーーー┛     ┗ーーーーーーーーーーーーー┛    ┗ーーーーーーーーーーーーー┛    ┗ーーーーーーーーーーーーー┛");
    }
    
    //////////////////////////// -Interactions Coffres- ///////////////////////////////////*

    boolean estUnCoffre(char[][] labyrinthe, Joueur j, String deplacement){
        int[] positionFuture = positionFuture(deplacement, j);
        if (labyrinthe[positionFuture[0]][positionFuture[1]] == 'C'){
            return true;
        }
        return false;
    }

    boolean estReponseCoffre(String reponse, String proposition, Joueur j){
        boolean bool = false;
        if(!equals(reponse,proposition)){
            println();
            println(RED+"Mauvaise Réponse ! Le coffre va disparaitre"+RESET);
            println();
        }
        else{
            println();
            println(GREEN+"Bonne Réponse ! Le coffre s'ouvre. "+RESET);
            println();
            if(j.nbVies!=3){
                j.nbVies++;
            }
            bool = true;
        }
        return bool;
    }

    void rencontreCoffre(String homonymes, String motManquant, String phrase, Joueur j){
        Coffre c = creerCoffre(homonymes, motManquant, phrase);
        String proposition = "";
        afficherPropositionCoffre(c.phrase, c.homonymes);
        println();
        println(RED+"Quel est le bon mot correspondant à cette phrase ? Votre réponse doit être écrite en minuscule et sans espace."+RESET);
        println();
        proposition = readString();
        estReponseCoffre(c.motManquant, proposition, j);
    }

    void afficherPropositionCoffre(String phrase, String homonymes){
        println();
        println(BLUE+phrase+RESET);
        println();
        println(YELLOW+"Homonymes possibles :" + homonymes+RESET);
        println();
        afficherFichier(COFFRE);
    }


    //////////////////////////// -Affichage du contenu d'un fichier texte d'après son chemin- ///////////////////////////////////
    
    void afficherFichier(String chemin){

	    File unTexte = newFile(chemin);

	    //Stockage dans une variable de la ligne suivante dans le fichier

	    while(ready(unTexte)){
	        //affichage du contenu de la ligne suivante
	        println(readLine(unTexte));
	    }
    }

    void afficherVie(int vie){
        if(vie==1){
            print("             Vie : "+RED+ "❤"+RESET);
        }
        if(vie==2){
            print("             Vie : "+RED+ "❤❤"+RESET);
        }
        if(vie==3){
            print("             Vie : "+RED+ "❤❤❤"+RESET);
        }
    }

}