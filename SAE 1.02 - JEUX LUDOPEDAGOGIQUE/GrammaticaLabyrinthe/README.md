Grammatical Labyritnhe
===========

Développé par <Louis DORMAEL> <Killian LAWSON>
Contacts : <louis.dormael.etu@univ-lille.fr> , <killian.lawson.etu@univ-lille.fr>

# Présentation de Grammatical Labyritnhe

Vous êtes prisionnier d'un labyrinthe rempli de monstres et de grammaire. Allez-vous réussir à avoir les bonnes réponses pour vous échapper ?


# Utilisation de Grammatical Labyritnhe

Afin d'utiliser le projet, il suffit de taper les commandes suivantes dans un terminal :

```
./compile.sh
```
Permet la compilation des fichiers présents dans 'src' et création des fichiers '.class' dans 'classes'

```
./run.sh Grammaticalabyrinth
```
Permet le lancement du jeu
